---
layout: resume
title: About
permalink: /about/
---

{% include_relative resume/employment.html %}

{% include_relative resume/education.html %}

{% include_relative resume/skills.html %}

{% include_relative resume/interests.html %}
