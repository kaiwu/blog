---
layout: resume
title: 简历
permalink: /about_cn/
---

{% include_relative resume/employment-cn.html %}

{% include_relative resume/education-cn.html %}

{% include_relative resume/skills-cn.html %}

{% include_relative resume/interests-cn.html %}
